
	
	// 1.  Create a function which will be able to add two numbers.
	// 	-Numbers must be provided as arguments.
	// 	-Display the result of the addition in our console.
	// 	-function should only display result. It should not return anything.

	// 	Create a function which will be able to subtract two numbers.
	// 	-Numbers must be provided as arguments.
	// 	-Display the result of subtraction in our console.
	// 	-function should only display result. It should not return anything.

	// 	-invoke and pass 2 arguments to the addition function
	// 	-invoke and pass 2 arguments to the subtraction function

	function addTwo(a,b) {
		console.log("sum of " + a + " and " + b)
		console.log(a+b);
	}
	function subtractTwo(a,b) {
		console.log("difference of " + a + " and " + b)
		console.log(a-b);
	}
	addTwo(5,3);
	subtractTwo(5,3);

	// 2.  Create a function which will be able to multiply two numbers.
	// 		-Numbers must be provided as arguments.
	// 		-Return the result of the multiplication.

	// 	Create a function which will be able to divide two numbers.
	// 		-Numbers must be provided as arguments.
	// 		-Return the result of the division.

	//  	Create a global variable called outside of the function called product.
	// 		-This product variable should be able to receive and store the result of multiplication function.
	// 	Create a global variable called outside of the function called quotient.
	// 		-This quotient variable should be able to receive and store the result of division function.

	// 	Log the value of product variable in the console.
	// 	Log the value of quotient variable in the console.
	function multiplyTwo(a,b) {
		console.log("product of " + a + " and " + b)
		console.log(a*b);
	}
	function divideTwo(a,b) {
		console.log("quotient of " + a + " and " + b)
		console.log(a/b);
	}
	multiplyTwo(5,3);
	divideTwo(5,3);
	// 3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
	// 		-a number should be provided as an argument.
	// 		-look up the formula for calculating the area of a circle with a provided/given radius.
	// 		-look up the use of the exponent operator.
	// 		-you can save the value of the calculation in a variable.
	// 		-return the result of the area calculation.

	// 	Create a global variable called outside of the function called circleArea.
	// 		-This variable should be able to receive and store the result of the circle area calculation.

	// Log the value of the circleArea variable in the console.

	function areaFromRadius(r) {
		return Math.PI*r*r
	}
	console.log("Area of circle with 15 radius");
	console.log(areaFromRadius(15));

	// 4. 	Create a function which will be able to get total average of four numbers.
	// 		-4 numbers should be provided as an argument.
	// 		-look up the formula for calculating the average of numbers.
	// 		-you can save the value of the calculation in a variable.
	// 		-return the result of the average calculation.

	//     Create a global variable called outside of the function called averageVar.
	// 		-This variable should be able to receive and store the result of the average calculation
	// 		-Log the value of the averageVar variable in the console.
	let averageVar;
	function averageFour(a,b,c,d){
		return (a+b+c+d)/4;
	}
	averageVar = averageFour(1,4,55,7);
	console.log("average of 1, 4, 55 and 7 is " + averageVar);

	// 5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
	// 		-this function should take 2 numbers as an argument, your score and the total score.
	// 		-First, get the percentage of your score against the total. You can look up the formula to get percentage.
	// 		-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
	// 		-return the value of the variable isPassed.
	// 		-This function should return a boolean.

	// 	Create a global variable called outside of the function called isPassingScore.
	// 		-This variable should be able to receive and store the boolean result of the checker function.
	// 		-Log the value of the isPassingScore variable in the console.
let isPassed;
let isPassingScore;
	function didIPass(score, total) {
	let pct = score/total;
	if (pct> 0.75){
		isPassed = pct;
	}
	return pct > 0.75;
}
isPassingScore = didIPass(74,100);
console.log("i scored 74 out of 100 did i pass?");
console.log(isPassingScore)


